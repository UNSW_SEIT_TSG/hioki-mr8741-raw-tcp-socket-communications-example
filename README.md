# HIOKI MR8741 Raw TCP Socket Communications Example

Requires that the application runs on a machine connected on the same network as the MR8741.  In development a simple cross-over cable has been used with the ip addresses manually configured.

The configuration of the network needs to match that specified in the MR8741 interface (See menu at HIOKI>System>Interface).

<img src="images/PXL_20220412_042803078.jpg" alt="HIOKI>System>Interface" width="640"/>

Demonstration video: https://youtu.be/rjOsbpVXMCc.
