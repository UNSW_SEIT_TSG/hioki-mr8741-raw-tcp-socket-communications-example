from socket import socket
from socket import AF_INET
from socket import SOCK_STREAM

"""Based on example from https://wiki.python.org/moin/TcpCommunication"""

def t():
  expectation = "HIOKI,MR8741,211048673-1,V2.32\r\n"
  observation = f("*IDN?\n")
  result = observation == expectation
  if not result:
    print(f'expectation: {expectation}')
    print(f'observation: {observation}')
  return result

def f(message, address='192.168.0.2', port=8802, buffer_size=1024):
  s = socket(AF_INET, SOCK_STREAM)
  s.connect((address, port))
  s.send(message.encode('utf-8'))
  received_data = s.recv(buffer_size)
  received_string = received_data.decode('utf-8')
  s.close()
  return received_string

if __name__ == '__main__':
  print(f"Test Passed: {t()}")
