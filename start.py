# An example of a program calling the send_receive function.

from send_receive import f as send_receive

def main():
  response = send_receive("boo")
  print(response)

if __name__ == '__main__':
  main()
